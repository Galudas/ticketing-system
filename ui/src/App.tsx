import React, {Fragment} from 'react';
import {BrowserRouter, Route, Switch, useParams} from "react-router-dom";
import './App.css';
import Navbar from "./Navbar";
import '@fortawesome/fontawesome-free/css/all.min.css';
import
    'bootstrap-css-only/css/bootstrap.min.css';
import
    'mdbreact/dist/css/mdb.css';

const App: React.FC = () => {
    return (
        <Fragment>
            <BrowserRouter>
                <Navbar/>
                <Switch>
                </Switch>
            </BrowserRouter>
        </Fragment>
    );
};

export default App;
