CREATE TABLE public.Client
(
    id          varchar(50) not null unique primary key,
    name        varchar(50),
    phoneNumber numeric,
    email       varchar(50)
);

CREATE TABLE public.Operator
(
    id       varchar(50) not null unique primary key,
    email    varchar(50),
    password varchar(10),
    name     varchar(10),
    username varchar(30)
);

CREATE TABLE public.Resolution
(
    id   varchar(50) not null unique primary key,
    name varchar(50)
);

CREATE TABLE public.TicketLabel
(
    id        varchar(50) not null unique primary key,
    ticketId  varchar(10),
    labelName varchar(10)

);



