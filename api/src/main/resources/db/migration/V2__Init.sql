CREATE TABLE public.Ticket
(
    id           varchar(50) not null unique primary key ,
    date         varchar(50),
    statusId     varchar(10),
    resolutionId varchar(10),
    title        varchar(30),
    content      varchar(300),
    active       boolean
);
