package ticketing.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Resolution {
    String id;
    String name;
}
