package ticketing.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Client {
    String id;
    String name;
    Integer phoneNumber;
    String email;
}
