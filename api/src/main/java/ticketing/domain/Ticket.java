package ticketing.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Ticket {

    String id;
    String date;
    String statusId;
    String resolutionId;
    String clientId;
    String operatorId;
    String title;
    String content;
    boolean active;

}
