package ticketing.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Operator {
    String id;
    String email;
    String password;
    String name;
    String username;
}
