package ticketing.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TicketLabel {
    String id;
    String ticketId;
    String labelName;
}
